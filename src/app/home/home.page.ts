import { ChangeDetectorRef, Component } from '@angular/core';
import { Firestore, collection, collectionData, doc, docData, addDoc, deleteDoc, updateDoc } from '@angular/fire/firestore';
import { AlertController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {


  segment = 0;
  obj: any;
  found = [];
  notFound = [];
  toast: HTMLIonToastElement;

  constructor(
    private firestore: Firestore,
    private cd: ChangeDetectorRef,
    public alertController: AlertController,
    public toastController: ToastController,

  ) {
    // load all treasures
    this.getNotFound();
    // load  found treasures
    this.getFound();
  }

  segmentChanged(e: any) {
    this.segment = parseInt(e.detail.value, 10);
  }

  async presentAlertPrompt() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'New treasure',
      inputs: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Title'
        },
        {
          name: 'des',
          type: 'textarea',
          placeholder: 'description'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (res) => {
            this.createObject({ name: res.name, description: res.des })
          }
        }
      ]
    });

    await alert.present();
  }


  getNotFound() {
    this.getNf().subscribe(res => {
      this.notFound = res
      this.cd.detectChanges();
    });
  }

  getFound() {
    this.getF().subscribe(res => {
      this.found = res
      this.cd.detectChanges();
    });
  }

  async createObject(obj: any) {
    await this.add(obj)
    this.presentToast('New Tresure created!')
  }

  async deleteObject(obj: any) {
    await this.delete(obj)
    this.presentToast('Tresure deleted!')
  }



  // calls to firebase
  getNf() {
    const collectionRef = collection(this.firestore, 'treasure');
    return collectionData(collectionRef, { idField: 'id' });
  }

  getF() {
    const collectionRef = collection(this.firestore, 'found');
    return collectionData(collectionRef, { idField: 'id' });
  }

  add(treasure: any) {
    const collectionRef = collection(this.firestore, 'treasure');
    return addDoc(collectionRef, treasure);
  }

  delete(treasure: any) {
    const docRef = doc(this.firestore, `treasure/${treasure.id}`);
    return deleteDoc(docRef);
  }

  async presentToast(txt) {
    try {
      this.toast.dismiss();
    }
    catch (e) { }
    this.toast = await this.toastController.create({
      message: txt,
      duration: 2500,
      color: 'success',
      position: 'top'
    });
    this.toast.present();
  }

}
